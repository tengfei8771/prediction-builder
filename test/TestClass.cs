﻿using PredictionBuilder.Attributes;
using PredictionBuilder.Enum;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    internal class TestClass
    {
        [MappingTarget("base_courses.Id")]
        public int Id { get; set; }
        [Operator(OperatorType.Contains)]
        [CustomMethod(typeof(TestExtension), "TestMethod", OperatorType.GreaterThanOrEqual,0)]
        [Connect(ConnectType.And)]
        [MappingTarget("base_courses.Name", "base_video.VideoTitle")]
        [ParameterOrder(ParameterOrderType.Asc)]
        public string Name1 { get; set; }
        [Operator(OperatorType.Equal)]
        [Connect(ConnectType.Or)]
        [MappingTarget("base_courses.Infos", "base_authors.Name")]
        [ParameterOrder(ParameterOrderType.Desc)]
        public string TestName { get; set; }
        public int Status { get; set; }
        [MappingTarget("base_courses.Id")]
        [Operator(OperatorType.Contains)]
        public IEnumerable<int> ids { get; set; }
    }
    internal class TestClass1
    {
        public IEnumerable<int> TestList { get; set; }
    }
    internal class TestClass2
    {
        public string Name { get; set; }
    }
}
