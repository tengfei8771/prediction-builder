﻿// See https://aka.ms/new-console-template for more information
using PredictionBuilder.Enum;
using PredictionBuilder.Impl;
using PredictionBuilder.Interfaces;
using SqlSugar;
using System.Linq.Expressions;
using System.Reflection;
using test;
using test.Model;

SqlSugarClient sqlSugarClient = new SqlSugarClient(new ConnectionConfig()
{
    ConnectionString = "Server=localhost;Database=sz_test;Uid=root;Pwd=root;charset=utf8;port=3306;SslMode = none;",
    DbType = DbType.MySql,
    IsAutoCloseConnection = true,
    AopEvents = new AopEvents()
    {
        OnLogExecuted = (sql, p) =>
        {
            Console.WriteLine(sql);
        }
    }
});
TestClass queryModel = new TestClass()
{
    Id = 10,
    //Name1 = "测试",
    TestName = "testName",
    Status = 0,
    ids = Enumerable.Range(0, 100),
};
var bs = BitConverter.GetBytes(100);
PredictionCreator<TestClass> creator = new PredictionCreator<TestClass>(queryModel);
var exp = creator
    .CreateBuilder<base_catalogs, base_course_r_catalog, base_courses, base_video_r_course, base_videos, base_authors>(1, null)
    .SetAggregateCondition(ConnectType.Or,t=>t.TestName,t=>t.Id)
    .ToExpression();
var query = sqlSugarClient.Queryable<base_catalogs, base_course_r_catalog, base_courses, base_video_r_course, base_videos, base_authors>((a, b, c, d, e, f) =>
new JoinQueryInfos(
    JoinType.Inner, a.Id == b.CatalogId,
    JoinType.Inner, b.CourseId == c.Id,
    JoinType.Inner, c.Id == d.CourseId,
    JoinType.Inner, d.VideoId == e.Id,
    JoinType.Inner, e.AuthorId == f.Id
    )
).Where(exp).ToList();
//ParameterExpression parameterExpression = Expression.Parameter(typeof(TestClass1), "t");
//MemberExpression member = Expression.PropertyOrField(parameterExpression, "TestList");
//var type1 = typeof(Enumerable);
//var typeee = type1.GetMethods().Where(t => t.Name == "Count" && t.GetParameters().Count() == 1).FirstOrDefault().GetParameters().FirstOrDefault().ParameterType;
//var typeeee = typeof(IEnumerable<>);
//var method = Type.GetType("System.Linq.Enumerable, System.Linq, Version=7.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
//    .GetMethod("Count",new Type[] { typeof(IEnumerable<>) }).MakeGenericMethod(typeof(int));
//var test1 = Expression.Call(null, method, member);
//var exp = Expression.Lambda<Func<TestClass1, List<int>>>(test1, parameterExpression);
////var exp = Expression.Lambda<Func<TestClass, bool>>(Expression.Call(constant, method, member), parameterExpression);
Console.WriteLine("Hello, World!");
