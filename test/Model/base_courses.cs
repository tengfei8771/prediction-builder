﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///课程表
    ///</summary>
    [SugarTable("base_courses")]
    public partial class base_courses
    {
           public base_courses(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SortId {get;set;}

           /// <summary>
           /// Desc:课程缩略图--课程代码做课程缩略图文件名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CourseImage {get;set;}

           /// <summary>
           /// Desc:关键字
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KeyWords {get;set;}

           /// <summary>
           /// Desc:课程简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Infos {get;set;}

           /// <summary>
           /// Desc:是否置顶
           /// Default:b'0'
           /// Nullable:True
           /// </summary>           
           public bool? IsTop {get;set;}

           /// <summary>
           /// Desc:状态 0正常 -1删除
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UpdateTime {get;set;}

    }
}
