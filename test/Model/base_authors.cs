﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///讲师表
    ///</summary>
    [SugarTable("base_authors")]
    public partial class base_authors
    {
           public base_authors(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:讲师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:职称职位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Position {get;set;}

           /// <summary>
           /// Desc:讲师简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Infos {get;set;}

           /// <summary>
           /// Desc:讲师图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AuthorImage {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SortId {get;set;}

           /// <summary>
           /// Desc:是否是专家 1:专家  0:普通讲师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? IsExpert {get;set;}

           /// <summary>
           /// Desc:首字母
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Initial {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UpdateTime {get;set;}

    }
}
