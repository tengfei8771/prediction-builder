﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///课程和分类关联表
    ///</summary>
    [SugarTable("base_course_r_catalog")]
    public partial class base_course_r_catalog
    {
           public base_course_r_catalog(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CatalogId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CourseId {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SortId {get;set;}

           /// <summary>
           /// Desc:状态 -1删除 1关联未保存
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

           /// <summary>
           /// Desc:操作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UpdateTime {get;set;}

           /// <summary>
           /// Desc:发布时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PublishTime {get;set;}

    }
}
