﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///视频表
    ///</summary>
    [SugarTable("base_videos")]
    public partial class base_videos
    {
           public base_videos(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:正题名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoTitle {get;set;}

           /// <summary>
           /// Desc:视频代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoCode {get;set;}

           /// <summary>
           /// Desc:视频排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SortId {get;set;}

           /// <summary>
           /// Desc:来源于产品
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductFrom {get;set;}

           /// <summary>
           /// Desc:视频缩略图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoImage {get;set;}

           /// <summary>
           /// Desc:文件大小（MB）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? FileSize {get;set;}

           /// <summary>
           /// Desc:时长（秒）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? Duration {get;set;}

           /// <summary>
           /// Desc:视频简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoInfo {get;set;}

           /// <summary>
           /// Desc:视频关键词
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KeyWords {get;set;}

           /// <summary>
           /// Desc:是否已经下架 0：未下架  1：下架
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? IsUnderCarriage {get;set;}

           /// <summary>
           /// Desc:讲师
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? AuthorId {get;set;}

           /// <summary>
           /// Desc:入库时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:最后更改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UpdateTime {get;set;}

           /// <summary>
           /// Desc:发布时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? PublishTime {get;set;}

    }
}
