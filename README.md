# PredictionBuilder

#### 介绍
解析实体类，生成表达式。可用于linq以及使用Expression的ORM。

#### 软件架构
软件架构说明


#### 安装教程

1.  nuget 搜索 PredictionBuilder 并安装

#### 使用说明

1.定义QueryModel

```
//queryModel
internal class TestClass
{
    public string Id { get; set; }
    [Operator(OperatorType.Contains)]//内置运算类型特性，如果存在[CustomMethod]特性则不生效
    [CustomMethod(typeof(TestExtension), "TestMethod", OperatorType.GreaterThanOrEqual,0)]//自定义方法特性，需要声明orm拓展方法所在的类和方法(必须为静态public),同时根据定义的方法的返回值与预期值生成对应的表达式
    [Connect(ConnectType.And)]//条件连接符
    [MappingTarget("base_courses.Name", "base_video.VideoTitle")]//对应Type映射的属性
    [ParameterOrder(ParameterOrderType.Asc)]//参数顺序 生成的是a.Id==1还是1==a.Id
    public string Name1 { get; set; }
    [Operator(OperatorType.Equal)]
    [Connect(ConnectType.And)]
    [MappingTarget("base_courses.Infos", "base_authors.Name")]
    [ParameterOrder(ParameterOrderType.Desc)]
    public string TestName { get; set; }
    public int Status { get; set; }
}
```
2.创建查询实体

```
using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///讲师表
    ///</summary>
    [SugarTable("base_authors")]
    public partial class base_authors
    {
           public base_authors(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:讲师姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:职称职位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Position {get;set;}

           /// <summary>
           /// Desc:讲师简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Infos {get;set;}     
    }
}

using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///分类表
    ///</summary>
    [SugarTable("base_catalogs")]
    public partial class base_catalogs
    {
           public base_catalogs(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:父级Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ParentId {get;set;}

           /// <summary>
           /// Desc:0正常 -1删除
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

    }
}

using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///课程和分类关联表
    ///</summary>
    [SugarTable("base_course_r_catalog")]
    public partial class base_course_r_catalog
    {
           public base_course_r_catalog(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CatalogId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CourseId {get;set;}       
           public int? Status {get;set;}


    }
}

using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///课程表
    ///</summary>
    [SugarTable("base_courses")]
    public partial class base_courses
    {
           public base_courses(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:课程名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}
           /// <summary>
           /// Desc:课程简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Infos {get;set;}

           /// <summary>
           /// Desc:是否置顶
           /// Default:b'0'
           /// Nullable:True
           /// </summary>           
           public bool? IsTop {get;set;}

           /// <summary>
           /// Desc:状态 0正常 -1删除
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

    }
}

using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///视频和课程关联表
    ///</summary>
    [SugarTable("base_video_r_course")]
    public partial class base_video_r_course
    {
           public base_video_r_course(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? VideoId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CourseId {get;set;}
           /// <summary>
           /// Desc:0 正常 -1删除
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

    }
}

using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace test.Model
{
    ///<summary>
    ///视频表
    ///</summary>
    [SugarTable("base_videos")]
    public partial class base_videos
    {
           public base_videos(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:正题名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoTitle {get;set;}

           /// <summary>
           /// Desc:视频代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoCode {get;set;}

           /// <summary>
           /// Desc:视频排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SortId {get;set;}

           /// <summary>
           /// Desc:来源于产品
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductFrom {get;set;}

           /// <summary>
           /// Desc:视频缩略图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoImage {get;set;}

           /// <summary>
           /// Desc:文件大小（MB）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? FileSize {get;set;}

           /// <summary>
           /// Desc:时长（秒）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public double? Duration {get;set;}

           /// <summary>
           /// Desc:视频简介
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoInfo {get;set;}

    }
}

```
3.特性说明

```
此特性已经移除！此特性已经移除！此特性已经移除！将此特性拆分其他三个特性，目的是更好的支持生成自定义方法。
public ConditionAttribute(OperatorType operatorType= OperatorType.Equal,ConnectType connectType=ConnectType.And, params string[] mapList)
{
       
}

/// <summary>
/// 内置运算符特性
/// </summary>
/// <param name="operatorType">运算符类型</par
public OperatorAttribute(OperatorType operatorType= OperatorType.Equal)
{
    OperatorType = operatorType;
}

/// <summary>
/// 条件连接特性
/// </summary>
/// <param name="connectType">连接类型</param>
public ConnectAttribute(ConnectType connectType= ConnectType.And)
{
    ConnectType = connectType;
}

/// <summary>
/// 映射到对应类型的属性特性
/// </summary>
/// <param name="values">映射名称(支持全称、简称以及属性名)</param>
public MappingTargetAttribute(params string[] values)
{
    Values= values;
}

/// <summary>
/// 自定义方法特性
/// </summary>
/// <param name="methodType">静态方法所在类</param>
/// <param name="methodName">静态类名</param>
/// <param name="operatorType">操作符</param>
/// <param name="methodPredictValue">预期返回值</param>
public CustomMethodAttribute(Type methodType, string methodName, OperatorType operatorType, object methodPredictValue)
{
    MethodType = methodType;
    MethodName = methodName;
    OperatorType = operatorType;
    MethodPredictValue = methodPredictValue;
}


/// <summary>
/// ctor 使用此特性声明如果queryModel的属性值等于哪些值,则不生成表达式
/// </summary>
/// <param name="targetType">作为哪种类型判断</param>
/// <param name="values">忽略值集合(如果集合中有一条满足条件就会跳过条件构造)</param>
public SkipBuildAttribute(Type targetType=null,params object[] values)
{

}
/// <summary>
/// 声明参数的表达式左右顺序，例如a.Id==1或者1==a.Id
/// </summary>
/// <param name="parameterOrderType">排序枚举 默认正序</param>
public ParameterOrderAttribute(ParameterOrderType parameterOrderType = ParameterOrderType.Asc)
{

}

//支持的运算符如下:
    public enum OperatorType
    {
        /// <summary>
        /// 相等
        /// </summary>
        Equal,
        /// <summary>
        /// 不相等
        /// </summary>
        NotEqual,
        /// <summary>
        /// 小于
        /// </summary>
        LessThan,
        /// <summary>
        /// 小于等于
        /// </summary>
        LessThanOrEqual,
        /// <summary>
        /// 大于
        /// </summary>
        GreaterThan,
        /// <summary>
        /// 大于等于
        /// </summary>
        GreaterThanOrEqual,
        /// <summary>
        /// 包含
        /// </summary>
        Contains,
        /// <summary>
        /// 不包含
        /// </summary>
        NotContains,
        /// <summary>
        /// 已什么开始
        /// </summary>
        StartsWith,
        /// <summary>
        /// 不已什么开始
        /// </summary>
        NotStartsWith,
        /// <summary>
        /// 已什么结束
        /// </summary>
        EndsWith,
        /// <summary>
        /// 不已什么结束
        /// </summary>
        NotEndsWith
    }
其中从contains开始到NotEndsWith结束，要求属性必须为string类型,否则会抛出异常

//IPredictionBuilder 支持的api如下
SetParameterName(arg[])//设置自定义的参数名 默认a,b,c,d可以根据需求修改
SetAggregateCondition(arg[])//设置聚合条件,可以理解为这几个条件拼接的结果和其他的条件拼接
ToExpression()//生成表达式
```


2.简单应用

```
SqlSugarClient sqlSugarClient = new SqlSugarClient(new ConnectionConfig()
{
    ConnectionString= "Server=localhost;Database=sz_test;Uid=root;Pwd=root;charset=utf8;port=3306;SslMode = none;",
    DbType=DbType.MySql,
    IsAutoCloseConnection=true,
    AopEvents=new AopEvents()
    {
        OnLogExecuted = (sql, p) =>
        {
            Console.WriteLine(sql);
        }
    }
});
TestClass queryModel = new TestClass()
{
    Name1 = "测试",
    TestName="testName",
    Status=0
};
PredictionCreator<TestClass> creator = new PredictionCreator<TestClass>(queryModel);
var exp= creator
    .CreateBuilder<base_catalogs, base_course_r_catalog, base_courses, base_video_r_course, base_videos, base_authors>(1,null)//此参数为设置全局忽略条件,若queryModel的任意属性的值等于1或者null,则会跳过此条件的生成
    .ToExpression();
Expression<Func<base_courses, bool>> exp1 = t => t.CourseImage == "1" || t.Name == "test";
var query = sqlSugarClient.Queryable<base_catalogs, base_course_r_catalog, base_courses, base_video_r_course, base_videos, base_authors>((a, b, c, d, e, f) =>
new JoinQueryInfos(
    JoinType.Inner,a.Id==b.CatalogId,
    JoinType.Inner,b.CourseId==c.Id,
    JoinType.Inner,c.Id==d.CourseId,
    JoinType.Inner,d.VideoId==e.Id,
    JoinType.Inner,e.AuthorId==f.Id
    )
).Where(exp).ToList();
```
ps:最多支持Expression<Func<T-T9,bool>>个参数的表达式生成，如果需要更多,请继承 AbstractBuilder<TModel>类自行拓展