﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PredictionBuilder.Enum
{
    /// <summary>
    /// 参数排序顺序
    /// </summary>
    public enum ParameterOrderType
    {
        /// <summary>
        /// 正序
        /// </summary>
        Asc,
        /// <summary>
        /// 逆序
        /// </summary>
        Desc
    }
}
