﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PredictionBuilder.Enum
{
    /// <summary>
    /// 条件连接类型
    /// </summary>
    public enum ConnectType
    {
        /// <summary>
        /// 并且
        /// </summary>
        And,
        /// <summary>
        /// 或
        /// </summary>
        Or
    }
}
