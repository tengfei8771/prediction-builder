﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PredictionBuilder.Enum
{
    /// <summary>
    /// 运算类型枚举
    /// </summary>
    public enum OperatorType
    {
        /// <summary>
        /// 相等
        /// </summary>
        Equal,
        /// <summary>
        /// 不相等
        /// </summary>
        NotEqual,
        /// <summary>
        /// 小于
        /// </summary>
        LessThan,
        /// <summary>
        /// 小于等于
        /// </summary>
        LessThanOrEqual,
        /// <summary>
        /// 大于
        /// </summary>
        GreaterThan,
        /// <summary>
        /// 大于等于
        /// </summary>
        GreaterThanOrEqual,
        /// <summary>
        /// 包含
        /// </summary>
        Contains,
        /// <summary>
        /// 不包含
        /// </summary>
        NotContains,
        /// <summary>
        /// 已什么开始
        /// </summary>
        StartsWith,
        /// <summary>
        /// 不已什么开始
        /// </summary>
        NotStartsWith,
        /// <summary>
        /// 已什么结束
        /// </summary>
        EndsWith,
        /// <summary>
        /// 不已什么结束
        /// </summary>
        NotEndsWith
    }
}
