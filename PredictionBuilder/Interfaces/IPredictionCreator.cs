﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace PredictionBuilder.Interfaces
{
    /// <summary>
    /// 创建者接口
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionCreator<TModel> where TModel : class, new()
    {
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, TModel> CreateBuilder<T>(params object[] ignoreValues) where T : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, TModel> CreateBuilder<T, T1>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, TModel> CreateBuilder<T, T1, T2>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, TModel> CreateBuilder<T, T1, T2, T3>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, TModel> CreateBuilder<T, T1, T2, T3, T4>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, TModel> CreateBuilder<T, T1, T2, T3, T4, T5>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6, T7>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new()
            where T7 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <typeparam name="T8"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new()
            where T7 : class, new()
            where T8 : class, new();
        /// <summary>
        /// 创建一个表达式构建者
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <typeparam name="T8"></typeparam>
        /// <typeparam name="T9"></typeparam>
        /// <param name="ignoreValues">如果queryModel的属性包含这里面的任意一个值,将被忽略</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, T9, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, T9>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new()
            where T7 : class, new()
            where T8 : class, new()
            where T9 : class, new();
    }
}
