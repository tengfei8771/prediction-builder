﻿using PredictionBuilder.Enum;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace PredictionBuilder.Interfaces
{
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, TModel> where T : class, new() where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName">参数名</param>
        /// <returns></returns>
        IPredictionBuilder<T, TModel> SetParameterName(string parameterName);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, TModel>
        where T : class, new()
        where T1 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName">参数名</param>
        /// <param name="parameterName1">第二个参数名</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, TModel> SetParameterName(string parameterName, string parameterName1);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, TModel> SetParameterName(string parameterName, string parameterName1, string parameterName2);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, T3, TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <param name="parameterName3"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, TModel> SetParameterName(string parameterName, string parameterName1, string parameterName2,string parameterName3);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, T3, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, T3, T4, TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <param name="parameterName3"></param>
        /// <param name="parameterName4"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, TModel> SetParameterName(string parameterName, string parameterName1, string parameterName2, string parameterName3,string parameterName4);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, T3, T4, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <typeparam name="T5"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, T3, T4, T5, TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <param name="parameterName3"></param>
        /// <param name="parameterName4"></param>
        /// <param name="parameterName5"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, TModel> SetParameterName(string parameterName, string parameterName1, string parameterName2, string parameterName3, string parameterName4,string parameterName5);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, T3, T4, T5, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <typeparam name="T5"></typeparam>
    /// <typeparam name="T6"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, T3, T4, T5,T6, TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
        where T6 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <param name="parameterName3"></param>
        /// <param name="parameterName4"></param>
        /// <param name="parameterName5"></param>
        /// <param name="parameterName6"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5,T6, TModel> SetParameterName(string parameterName, string parameterName1, 
            string parameterName2, string parameterName3, string parameterName4, string parameterName5,string parameterName6);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5,T6, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, T3, T4, T5,T6, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <typeparam name="T5"></typeparam>
    /// <typeparam name="T6"></typeparam>
    /// <typeparam name="T7"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, T3, T4, T5, T6,T7, TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
        where T6 : class, new()
        where T7 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <param name="parameterName3"></param>
        /// <param name="parameterName4"></param>
        /// <param name="parameterName5"></param>
        /// <param name="parameterName6"></param>
        /// <param name="parameterName7"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6,T7, TModel> SetParameterName(string parameterName, string parameterName1,
            string parameterName2, string parameterName3, string parameterName4, string parameterName5,string parameterName6,
            string parameterName7);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6,T7, TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, T3, T4, T5, T6,T7, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <typeparam name="T5"></typeparam>
    /// <typeparam name="T6"></typeparam>
    /// <typeparam name="T7"></typeparam>
    /// <typeparam name="T8"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7,T8,TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
        where T6 : class, new()
        where T7 : class, new()
        where T8 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <param name="parameterName3"></param>
        /// <param name="parameterName4"></param>
        /// <param name="parameterName5"></param>
        /// <param name="parameterName6"></param>
        /// <param name="parameterName7"></param>
        /// <param name="parameterName8"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7,T8, TModel> SetParameterName(string parameterName, string parameterName1,
            string parameterName2, string parameterName3, string parameterName4, string parameterName5, string parameterName6,
            string parameterName7,string parameterName8);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7,T8,TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, T3, T4, T5, T6, T7,T8, bool>> ToExpression();
    }
    /// <summary>
    /// 建造者接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <typeparam name="T5"></typeparam>
    /// <typeparam name="T6"></typeparam>
    /// <typeparam name="T7"></typeparam>
    /// <typeparam name="T8"></typeparam>
    /// <typeparam name="T9"></typeparam>
    /// <typeparam name="TModel"></typeparam>
    public interface IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8,T9, TModel>
        where T : class, new()
        where T1 : class, new()
        where T2 : class, new()
        where T3 : class, new()
        where T4 : class, new()
        where T5 : class, new()
        where T6 : class, new()
        where T7 : class, new()
        where T8 : class, new()
        where T9 : class, new()
        where TModel : class, new()
    {
        /// <summary>
        /// 设置表达式参数名
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterName1"></param>
        /// <param name="parameterName2"></param>
        /// <param name="parameterName3"></param>
        /// <param name="parameterName4"></param>
        /// <param name="parameterName5"></param>
        /// <param name="parameterName6"></param>
        /// <param name="parameterName7"></param>
        /// <param name="parameterName8"></param>
        /// <param name="parameterName9"></param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8,T9, TModel> SetParameterName(string parameterName, string parameterName1,
            string parameterName2, string parameterName3, string parameterName4, string parameterName5, string parameterName6,
            string parameterName7, string parameterName8,string parameterName9);
        /// <summary>
        /// 设置聚合条件
        /// </summary>
        /// <param name="innerJoinType">聚合条件内每个条件的连接符</param>
        /// <param name="outerJoinType">聚合条件和其他条件的连接符</param>
        /// <param name="selectors">聚合条件</param>
        /// <returns></returns>
        IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8,T9,TModel> SetAggregateCondition(ConnectType outerJoinType = ConnectType.And, params Expression<Func<TModel, object>>[] selectors);
        /// <summary>
        /// 生成表达式
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, T1, T2, T3, T4, T5, T6, T7, T8,T9, bool>> ToExpression();
    }
}
