﻿using PredictionBuilder.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PredictionBuilder.Attributes
{
    /// <summary>
    /// 条件连接特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ConnectAttribute:Attribute
    {
        /// <summary>
        /// 连接类型
        /// </summary>
        public ConnectType ConnectType { get; }
        /// <summary>
        /// 条件连接特性
        /// </summary>
        /// <param name="connectType">连接类型</param>
        public ConnectAttribute(ConnectType connectType= ConnectType.And)
        {
            ConnectType = connectType;
        }
    }
}
