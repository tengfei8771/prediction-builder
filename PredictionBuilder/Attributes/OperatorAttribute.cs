﻿using PredictionBuilder.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PredictionBuilder.Attributes
{
    /// <summary>
    /// 内置运算符特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class OperatorAttribute: Attribute
    {
        /// <summary>
        /// 操作符枚举
        /// </summary>
        public OperatorType OperatorType { get;}
        /// <summary>
        /// 内置运算符特性
        /// </summary>
        /// <param name="operatorType">运算符类型</param>
        public OperatorAttribute(OperatorType operatorType= OperatorType.Equal)
        {
            OperatorType = operatorType;
        }
    }
}
