﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PredictionBuilder.Attributes
{
    /// <summary>
    /// 映射到对应类型的属性特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class MappingTargetAttribute:Attribute
    {
        /// <summary>
        /// 映射集合
        /// </summary>
        public string[] Values { get;}
        /// <summary>
        /// 映射到对应类型的属性特性
        /// </summary>
        /// <param name="values">映射名称(支持全称、简称以及属性名)</param>
        public MappingTargetAttribute(params string[] values)
        {
            Values= values;
        }
    }
}
