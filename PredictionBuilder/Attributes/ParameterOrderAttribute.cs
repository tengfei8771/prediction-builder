﻿using PredictionBuilder.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PredictionBuilder.Attributes
{
    /// <summary>
    /// 声明参数的表达式左右顺序，例如a.Id==1或者1==a.Id
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ParameterOrderAttribute : Attribute
    {
        /// <summary>
        /// 参数排序
        /// </summary>
        public ParameterOrderType ParameterOrderType { get; }
        /// <summary>
        /// 声明参数的表达式左右顺序，例如a.Id==1或者1==a.Id
        /// </summary>
        /// <param name="parameterOrderType">排序枚举 默认正序</param>
        public ParameterOrderAttribute(ParameterOrderType parameterOrderType = ParameterOrderType.Asc)
        {
            ParameterOrderType = parameterOrderType;
        }
    }
}
