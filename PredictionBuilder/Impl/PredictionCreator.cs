﻿using PredictionBuilder.Interfaces;
using System;

namespace PredictionBuilder.Impl
{
    public class PredictionCreator<TModel> : IPredictionCreator<TModel> where TModel : class, new()
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="model"></param>
        public PredictionCreator(TModel model)
        {
            this.model = model;
        }
        private TModel model;
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, TModel> CreateBuilder<T>(params object[] ignoreValues)
            where T : class, new()
        {
            return new PredictionBuilder<T, TModel>(model,ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, TModel> CreateBuilder<T, T1>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
        {
            return new PredictionBuilder<T, T1, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, TModel> CreateBuilder<T, T1, T2>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, T3, TModel> CreateBuilder<T, T1, T2, T3>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, T3, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, T3, T4, TModel> CreateBuilder<T, T1, T2, T3, T4>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, T3, T4, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, T3, T4, T5, TModel> CreateBuilder<T, T1, T2, T3, T4, T5>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, T3, T4, T5, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, T3, T4, T5, T6, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6, T7>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new()
            where T7 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <typeparam name="T8"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new()
            where T7 : class, new()
            where T8 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, TModel>(model, ignoreValues);
        }
        /// <summary>
        /// 创建一个builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <typeparam name="T8"></typeparam>
        /// <typeparam name="T9"></typeparam>
        /// <param name="ignoreValues">忽略值集合</param>
        /// <returns></returns>
        public IPredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, T9, TModel> CreateBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, T9>(params object[] ignoreValues)
            where T : class, new()
            where T1 : class, new()
            where T2 : class, new()
            where T3 : class, new()
            where T4 : class, new()
            where T5 : class, new()
            where T6 : class, new()
            where T7 : class, new()
            where T8 : class, new()
            where T9 : class, new()
        {
            return new PredictionBuilder<T, T1, T2, T3, T4, T5, T6, T7, T8, T9, TModel>(model, ignoreValues);
        }

        
    }
}
